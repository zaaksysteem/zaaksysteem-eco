# Zaaksysteem ECO System

This repo is used for creating releases and is also known as "Arne's famous script"

For a detailed instruction of how to create a new release, follow the instructions: https://mintlab.atlassian.net/wiki/spaces/ML/pages/713129999/Release+Process+Technical

As the zaaksysteen application is a microservices application, we need to pay some extra attention to releases, as most of the time many new versions of several services need to be deployed at release time.
This utility replaces some very boring tasks and can help you updates things in a more automated fashion.

The utilities in this repository add some helper tools for our release process, but do not replace the instructions as described on confluence, so make sure you know the ins and outs.


## Context

When a new git tag is pushed to a repository, for this tag a new container image is build from gitlab-ci and pushed to the gitlab container registry for said repository.
These containers are automagically deployed on the non production environments of our kubernetes clusters,
but to prevent all hell from breaking loose, the production environments are not automagically updated when a new container tag is pushed to the registry.

Instead these utilities help you to release the required images on our kubernetes cluster by creating a Merge Request on the kubernetes configurations.


## Prerequisites

Without git this tools will not work (and neither will you, so make you installed git already!)

The utilities in this repository rely heavily on [myrepos](https://myrepos.branchable.com/), which is installed on intialization.


## Setup

To get started, clone this repo to your disk and initialize:

```
git clone git@gitlab.com:zaaksysteem/zaaksysteem-eco.git
cd zaaksysteem-eco && bin/init
```

Now retrieve all repositories (as instructed in the output of the `init` utility:
```
mr update
```

... This clones a copy of all repositories in the current workdir. Do not change the locations: We use these repositories as a pristine repository so no open changes should occur.


Now before you continue: [read the instructions listed here](https://mintlab.atlassian.net/wiki/spaces/ML/pages/713129999/Release+Process+Technical#Making-our-code-%E2%80%9Cdevops-ready%E2%80%9D)


## Usage

Except for the `bin/init`, none of the utilities in `bin/` are made for direct execution on the command line: they are executed as part of a larger routine that is defined in the `myrepos` configuration.

